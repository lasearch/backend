import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from mongoengine import connect

from routes import app as core_api


connect(
    os.getenv('MONGO_DATABASE', 'lasearch'),
    host=os.getenv('MONGO_HOST', 'localhost'),
    port=int(os.getenv('MONGO_PORT', 27017)),
    username=os.getenv('MONGO_USERNAME', None),
    password=os.getenv('MONGO_PASSWORD', None)
)


app = FastAPI(
    title='lasearch',
    description='liza alert search backend endpoints'
)
app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        expose_headers=["*"],
    )

app.mount('/api', core_api)


@app.get('/ping')
def ping():
    return {'success': True, 'message': 'pong'}