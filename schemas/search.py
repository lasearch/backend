from typing import List

from pydantic import  BaseModel


class RearangeDTO(BaseModel):
    tags: List[str]
    ids: List[str]