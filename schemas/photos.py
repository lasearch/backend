from typing import List
from xmlrpc.client import boolean

from pydantic import BaseModel


class SubmitPhotoDTO(BaseModel):
    photo_url: str
    tags: List[str]
    embedding: List[float]


class UploadedPhotoResponse(BaseModel):
    photo_url_presigned: str
    photo_url: str
    tags: List[str]
    custom_tags: List[str]
    embedding: List[float]

class UploadResponse(BaseModel): 
    success: boolean
    data: List[UploadedPhotoResponse]


class SubmitResponse(BaseModel):
    success: boolean
    id: str


class SubmitResponseBatch(BaseModel):
    success: boolean
    ids: List[str]


class UpdatePhotoDTO(BaseModel):
    tags: List[str]