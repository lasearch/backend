from typing import List

from models.custom_tags import CustomTags


def get_all_tags() -> List[str] :
    return list(map(lambda x: x['name'], CustomTags.objects))