from bson import ObjectId
from services import s3_service, cv_engine_service

from schemas.photos import *
from services import s3_service, elastic_service
from models.photos import Photos
from models.custom_tags import CustomTags


def upload_photo(filename: str, photo: bytes) -> UploadedPhotoResponse:
    tags, embedding, custom_tags = cv_engine_service.process_photo(photo)

    photo_url = s3_service.put_photo(filename, photo)

    index_tags(custom_tags)
    return {
        'photo_url_presigned': s3_service.get_presigned_url(photo_url),
        'photo_url': photo_url,
        'tags': tags,
        'custom_tags': custom_tags,
        'embedding': embedding
    }


def save_photo(data: SubmitPhotoDTO) -> str:
    mongo_photo = Photos(**data.dict())
    mongo_photo.save()

    elastic_service.index_object({
        'photo_url': data.photo_url,
        'tags': data.tags,
        'embedding': data.embedding
    }, mongo_photo.id)


    return str(mongo_photo.id)


def update_photo(id: str, tags: List[str]):
    photo = Photos.objects(id=ObjectId(id)).first()
    if photo:
        photo.tags = tags
        photo.save()


def get_photos():
    print(Photos.objects)


def index_tags(custom_tags: List[str]):
    for tag in custom_tags:
        if len(CustomTags.objects(name=tag)) == 0:
            db_tag = CustomTags(name=tag)
            db_tag.save()
    
