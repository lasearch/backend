from itertools import groupby
from statistics import mean
from typing import List, Tuple

import numpy as np

from models.photos import Photos
from services import s3_service, elastic_service, cv_engine_service

def search_by_tags(tags: List[str], page: int, limit: int) -> Tuple[List[dict], int]:
    results, total = elastic_service.search_tags(tags, page, limit)
    return list(map(
        lambda x: {
            'photo_url': s3_service.get_presigned_url(x['_source']['photo_url']), 
            'tags': x['_source']['tags'],
            'id': x['_id']}, 
        results)), total


def get_matching_tags_number(photo: Photos, tags: List[str]) -> int:
    photo_tags = set(photo['tags'])
    tags = set(tags)

    return len(tags.intersection(photo_tags))


def search_by_photo(photo: bytes, page: int, limit: int) -> Tuple[List[dict], int]: # todo: typing
    _, embedding, _ = cv_engine_service.process_photo(photo)

    results, total = elastic_service.search_vector(embedding, page, limit)
    mapped_results = list(map(
        lambda x: {
            'photo_url': s3_service.get_presigned_url(x['_source']['photo_url']), 
            'tags': x['_source']['tags'],
            'id': x['_id'],
            'score': round(x['_score'], 3)    
        }, 
        results))
    
    groupped_results = [list(it) for k, it in groupby(mapped_results, lambda x: x['score'])]    
    flattened_results = [i[0] if len(i) == 1 else i for i in groupped_results]

    return flattened_results, total


def rearange_images(tags: List[str], ids: List[str], page: int, limit: int) -> Tuple[List[dict], int]:
    photos = Photos.objects(id__in=ids)

    vectors = []
    for photo in photos:
        vectors.append(photo.embedding)
    mean_vector = np.mean(vectors, 0).tolist()

    results, total = elastic_service.search_vector(
        mean_vector, page, limit,
        {
            'bool': {
                'should': {
                    'terms': {
                        'tags': tags
                    }
                }
            }
        }
    )
    mapped_results = list(map(
        lambda x: {
            'photo_url': s3_service.get_presigned_url(x['_source']['photo_url']), 
            'tags': x['_source']['tags'],
            'id': x['_id'],
            'score': round(x['_score'], 3)    
        }, 
        results))

    return mapped_results, total
    
