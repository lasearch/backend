from mongoengine import *

from services.elastic_service import *
from models.photos import Photos

connect(
    os.getenv('MONGO_DATABASE', 'lasearch'),
    host=os.getenv('MONGO_HOST', 'localhost'),
    port=int(os.getenv('MONGO_PORT', 27017)),
    username=os.getenv('MONGO_USERNAME', None),
    password=os.getenv('MONGO_PASSWORD', None)
)


def migrate():
    mapping = {
        'properties': {
            'photo_url': {
                'type': 'keyword'
            },
            'tags': {
                'type': 'keyword'
            },
            'embedding': {
                'type': 'dense_vector',
                'dims': 256
            }
        }  
    }
    create_index('lasearch', mapping)

    photos = Photos.objects
    k = 0

    for photo in photos:
        doc = {
            'photo_url': photo.photo_url,
            'tags': photo.tags,
            'embedding': photo.embedding
        }
        doc_id = str(photo.id)

        try:
            index_object(doc, doc_id)
        except Exception as e:
            print(doc_id, e)            

        k += 1
        if k % 100 == 0:
            print(f'Processed {k} photos')


if __name__ == '__main__':
    migrate()