import os
from typing import List, Tuple

from elasticsearch import Elasticsearch

elastic_client = Elasticsearch(
    os.getenv('ELASTIC_URL', 'http://localhost:9200')
)


def index_object(data: dict, id: int):
    resp = elastic_client.index(index="lasearch", id=id, document=data)
    print(resp['result'])
    return resp['result']


def search_vector(embedding: List[float], page: int, limit: int, query: dict = {'match_all': {}}) -> Tuple[List[dict], int]:
    query = {
        'query': {
            'script_score': {
                'query': {
                    'match_all': {}
                },
                'script': {
                    'source': '(1.0 + cosineSimilarity(params.query_vector, \'embedding\'))',
                    'params': {
                        'query_vector': embedding
                    }
                },
                'min_score': 0
            }
        }
    }
    result = elastic_client.search(index='lasearch', body=query, size=1, sort={'_score': 'desc'})

    top_score = result['hits']['hits'][0]['_score']

    query['query']['script_score']['min_score'] = top_score * 0.9

    result = elastic_client.search(index='lasearch', body=query, size=limit, from_=limit*(page-1), sort={'_score': 'desc'})
    total = elastic_client.count(index='lasearch', body=query)['count']

    return result['hits']['hits'], total



def search_tags(tags: List[str], page: int, limit: int) -> Tuple[List[dict], int]:
    query = {
        'query': {
            'bool': {
                'should': [{'match': {'tags': i}} for i in tags]
            }
        }
    }
    result = elastic_client.search(index='lasearch', body=query, size=limit, from_=limit*(page-1), sort={'_score': 'desc'})
    total = elastic_client.count(index='lasearch', body=query)['count']

    return result['hits']['hits'], total


def create_index(index_name, mapping):
    elastic_client.indices.create(index=index_name, ignore=400, mappings=mapping)