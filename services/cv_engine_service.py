import json
import requests

from typing import List

class_mappings = {
    "Время суток": ["День", "Ночь", "Рассвет/Закат"],
    "Время года": ["Зима", "Весна", "Лето", "Осень"],
    "Местность": ["Лес", "Город"],
    "Авиа": ["Нет", "Да"],
    "Автомобили": ["Нет", "Да"],
    "БПЛА": ["Нет", "Да"],
    "Водолаз": ["Нет", "Да"],
    "Кинолог": ["Нет", "Да"],
    "Кони": ["Нет", "Да"],
    "Объятия": ["Нет", "Да"],
    "Шерп": ["Нет", "Да"]
}

class_names = [
    "Время суток",
    "Время года",
    "Местность",
    "Авиа",
    "Автомобили",
    "БПЛА",
    "Водолаз",
    "Кинолог",
    "Кони",
    "Объятия",
    "Шерп"
]


def process_photo(photo: bytes) -> List[str]:
    res = requests.post(
        'http://51.250.19.21:7766/get_features_and_tags',
        files={'file': photo}
        )

    print(res.status_code)
    print(res)

    json_res = res.json()
    embedding = json_res['features']
    labels = json_res['labels']
    tags = []

    for idx, label in enumerate(labels):
        if label == 'Да':
            tags.append(class_names[idx].replace('/', '-'))
        elif label == 'Нет' or label == 'Невозможно определить':
            continue
        else:
            label = label.replace('/', '-')
            tags.append(label)

    res = requests.post(
        'http://51.250.19.21:7766/get_detection_labels',
        files={'file': photo}
        )
    json_res = res.json()
    detection_labels = map(lambda x: x['name'].replace('/', '-').capitalize(), json_res['labels'])
    print(detection_labels)

    additional_labels = list(set(detection_labels))


    
    return tags, embedding, additional_labels