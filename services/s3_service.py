import os
import binascii

import boto3

s3_client = boto3.client(
    's3',
    endpoint_url=os.getenv('S3_ENDPOINT'),
    aws_access_key_id=os.getenv('S3_KEY_ID'),
    aws_secret_access_key=os.getenv('S3_KEY'),
    region_name=os.getenv('S3_REGION')
)

def put_photo(filename: str, photo: bytes, prefix: str = '') -> str:
    if not prefix:  # todo: clarify directory structure
        prefix = binascii.b2a_hex(os.urandom(15)).decode('utf-8')
    print(prefix)

    key = f'{prefix}/{filename}'

    s3_client.put_object(Bucket=os.getenv('S3_BUCKET'), Key=key, Body=photo)
    return key


def get_presigned_url(path: str) -> str:
    return s3_client.generate_presigned_url('get_object', ExpiresIn=86400, Params={'Bucket': os.getenv('S3_BUCKET'), 'Key': path})
