from typing import List

from fastapi import APIRouter, Query

from controllers import tags_controller


router = APIRouter(
    prefix='/tags',
    tags=['tags']
)


@router.get('/')
def get_all_tags():
    return {'success': True, 'data': tags_controller.get_all_tags()}