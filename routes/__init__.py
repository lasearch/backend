from fastapi import FastAPI

from routes.search import router as search_router
from routes.photos import router as photos_router
from routes.tags import router as tags_router


app = FastAPI(
    title='lasearch',
    description='liza alert search backend endpoints'
)
app.include_router(search_router)
app.include_router(photos_router)
app.include_router(tags_router)