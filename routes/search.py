from typing import List

from fastapi import APIRouter, Query, UploadFile

from controllers import search_controller
from schemas.search import *


router = APIRouter(
    prefix='/search',
    tags=['search']
)


@router.get('/tags')
def search_by_tags(page: int, limit: int, tags: List[str] = Query(default=[])):
    data, total = search_controller.search_by_tags(tags, page, limit)
    return {'data': data, 'total': total, 'page': page, 'limit': limit}


@router.post('/photo')
def search_by_image(photo: UploadFile, page: int, limit: int):
    results, total = search_controller.search_by_photo(photo.file.read(), page, limit)

    return {'success': True, 'data': results, 'total': total, 'page': page, 'limit': limit}


@router.post('/rearange')
def rearange_photos(data: RearangeDTO, page: int, limit: int):
    results, total = search_controller.rearange_images(data.tags, data.ids, page, limit)

    return {'success': True, 'data': results, 'total': total, 'page': page, 'limit': limit}
