from typing import List

from fastapi import APIRouter, UploadFile 

from controllers import photos_controller
from schemas.photos import *


router = APIRouter(
    prefix='/photos',
    tags=['photos']
)


@router.post('/upload', response_model=UploadResponse)
def upload_photo(
    photo: UploadFile
):
    content = photo.file.read()
    
    response = photos_controller.upload_photo(
        photo.filename,
        content
    )
    
    return {'success': True, 'data': [response]}


@router.post('/upload/batch', response_model=UploadResponse)
def upload_photo_batch(
    photos: List[UploadFile]
):
    print(photos)

    results = []

    for photo in photos:
        content = photo.file.read()
    
        results.append(photos_controller.upload_photo(
            photo.filename,
            content
        ))

    return {'success': True, 'data': results}


@router.post('/submit', response_model=SubmitResponse)
def submit_photo(data: SubmitPhotoDTO):
    result = photos_controller.save_photo(data)
    return {'success': True, 'id': result}


@router.post('/submit/batch', response_model=SubmitResponseBatch)
def submit_photo(data: List[SubmitPhotoDTO]):
    results = []
    for entry in data:
        result = photos_controller.save_photo(entry)
        results.append(result)
    return {'success': True, 'ids': results}


@router.get('/')
def get_photos():
    photos_controller.get_photos()
    return {'test': 1}


@router.put('/{id}')
def update_photo(id: str, data: UpdatePhotoDTO):
    photos_controller.update_photo(id, data.tags)
    return {'success': True}