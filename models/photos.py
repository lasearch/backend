import datetime

from mongoengine import *

class Photos(Document):
    photo_url = StringField(max_length=250, required=True)
    tags = ListField(field=StringField())
    embedding = ListField(field=FloatField())
    created_at = DateTimeField(default=datetime.datetime.utcnow)
