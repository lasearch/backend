import datetime

from mongoengine import *

class CustomTags(Document):
    name = StringField()
    created_at = DateTimeField(default=datetime.datetime.utcnow)
